// // EXTERNAL JavaScript
//
// // single line comment
//
// /
// this
// is
// a
// multi
// line
// comment
//  /
//
// // console.log()
// console.log(5 * 100);
//
// console.log("hello");
//
// console.log(true);
// console.log(false);
//
//
// // DATA TYPES
// // number, string, boolean, undefined, null... object
//
// // VARIABLES
// // let, const, var (most common used)
//
// // DECLARING VARIABLES
// // syntax/structure: var nameOfTheVariable;
// // syntax/structure: var nameOfVariable = value;
//
// // DECLARE A VARIABLE NAMED firstName
// var firstName;
//
// // ASSIGN A VALUE OF "Billy" TO firstName
// firstName = "Billy";
//
// // CALLED THE VARIABLE NAME
// console.log(firstName);
//
// // DECLARE A VARIABLE NAMED age AND
// // ASSIGN A VALUE OF 30 TO age
// // var age = 30;
//
// // CALL THE VARIABLE NAME age
// // console.log(age);
//
//
// // OPERATORS
// // Mathematical Arithmetic
//
// /*
// addition +
// subtraction -
// multiply *
// divide /
// modulus AKA remainder  %
//  */
//
// console.log( 12 / 4);
//
// console.log( 12 % 5);
// console.log( 12 % 4);
//
// console.log( (1 + 2) * 4 / 5);
// // PEMDAS
// // () ^ * / + -
//
// // LOGICAL OPERATORS
// /*
//
// AND &&
// OR  ||
// NOT !
//
// NOTE: returns a boolean value when used with boolean values
// also used with non-boolean values
//
// AND &&
// TRUE STATEMENT && TRUE STATEMENT (true)
// TRUE STATEMENT && FALSE STATEMENT (false)
// FALSE STATEMENT && TRUE STATEMENT (FALSE)
//     F && F ?
//
//  */
// console.log(true && true); // true
// console.log(true && false); // false
// console.log(false && true); // false
// console.log(false && false); // false
//
// // OR  ||
// // T || F ?
// console.log(true || false); // TRUE
// // T || T ?
// console.log(true || true); // true
// console.log( false || false); // false
// console.log( false || true ); // true
//
//
// // NOT  !
// // the opposite
// // !true = false
// // !false = true
//
// console.log(!false); // true
//
// console.log(!!!!!!!true);
//
// // COMPARISON OPERATOR
// // ==, ===, <, >, <=, >=
//
// // = used to ASSIGN value(s) to a variable
// var lastName = "Smith";
//
// // == checks if the VALUE is the same
// // === checks if the VALUE AND DATA TYPE are the same
//
// // better example of == vs ===
// var entry = 12;
// var entry2 = "12";
//
// console.log(entry == entry2); // true
// console.log(entry === entry2); // false
//
// // != , !==
// // != checks if the VALUE is NOT the same
// // !== checks if the VALUE AND DATA TYPE are NOT the same
//
// console.log(entry != entry2); // false
// console.log(entry !== entry2); // true
//
// // <, >, <=, >=
// const age = 21;
//
// console.log(age >= 21 || age < 18); //true
// //              T     ||     F      = True
//
// console.log(age <= 21 || age > 18); // true
// //              T     ||     T      = true
//
// console.log( age < 21 && age > 18); // false
// //              F     &&     T      = false

//my incomplete notes:

// */
// LOGICAL OPERATORS
// AND &&
// OR ||
// NOT !
//
// // AND &&
// console.log(true && true); // true
// console.log(true && false); // false
// console.log(false && true); // false
// console.log(false && false); // false
//
// // OR ||
// console.log(true || true); // true
// console.log(true || false); // true
// console.log(false || true); // true
// console.log(false || false); // false
//
// // NOT !
// //THE OPPOSITE
// console.log(!false); // true
// console.log(!true); // false
//
// //COMPARISON OPERATOR
// // ==, ===, <, >, <=, >=
//
// // = used to assign values to a variable
// var lastname = "Smith";
//
// // == checks if the value is the same
// // === checks if the value and data type are the same ("strictly equal")
//
// var entry = 12;
// var entry2 = "12";
//
// console.log(entry == entry2); // true
// console.log(entry === entry2); // false
//
// // !=, !==
// // != checks if the value is not the same
// // !== checks if the value and data type are not the same
//
// console.log(entry != entry2); // false
// console.log(entry !== entry2); // true
//
// // <, >, <=, >=
// const age = 21;
// console.log(age >= 21 || age < 18); // true
// console.log(age <= 21 || age > 18); // true
// console.log(age < 21 && age > 18); // false
//

const age = 21;
const person = "Bruce";
const person_age = 40;

//CONCATENATION
console.log("Hello my name is " + person + ".");
console.log("And I am " + person_age + " years old.");
console.log("Hello my name is " + person + " and I am " + person_age + " years old.");

//TEMPLATE STRINGS - use back ticks (above TAB key)
console.log(`Hello my name is ${person}.`);
console.log(`And I am ${person_age} years old.`);
console.log(`Hello my name is ${person} and I am ${person_age} years old.`)

//STRING METHODS
//HELPS US WITH STRING VARIABLES
//.lenght()
// gets the lenght of a string
console.log(person.length);
const greeting = "Hello World!";
console.log(greeting.length);

//.toUpperCase() / .toLowerCase()
// get the string in upper / lower case
console.log(greeting.toUpperCase());
console.log(greeting.toLowerCase());

// .substring()
// extracts character from a string between a "start" and "end" index not including
// the "end" itself

var goat ="Selena Quintanilla-Perez";

console.log(goat.substring(0,6));

console.log(goat.substring(7,18));

console.log(goat.substring(19));

//FUNCTIONS
// -a reusable block of code that performs a specified task

//syntax for function:
/*
function nameOfTheFunction() {
code you want to dfo
}
 */

//created a function named sayHello()
// function's body {}
function sayHello() {
    console.log("Hello!");
    alert ("Hello!");
}

//call a function
// sayHello();

//create a function with parameters
function addNumber(num1) {
    console.log(num1 + 2);
}

// call the function
addNumber(13);

//"NaN" means "Not a Number"

//create a function with two parameters

function message(name, age) {
    alert(`Hello my name is ${name} and I'm ${age} years old.`);
}

// message("Jose",54);

function petInfo(name, animal, age) {
    console.log("My pet's name is " + name + ". " + "He is a " + animal + ", and he is " + age + ".");
}

petInfo("Chico", "bulldog", 7);

//anonymous functions
//functions without a name, and stores it in a variable
//var nameOfVariable = function() {
//...code inside of function's body
// };