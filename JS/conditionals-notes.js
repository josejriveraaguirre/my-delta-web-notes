
// conditional statements
//if statement
//
// var numberOfLives = 0;
//
    // if (numberOfLives === 0) {
    //
    //     alert("Game Over!");
    //
    // }

//if else statement
//
// var score = 8;
//
//     if (score > 10) {
//
//         console.log("Nice job you scored more than 10!");
//
//     } else {
//
//         console.log("Try again loser!");
//
//     }

//if else if statement
//
// var age = 17;
    //
    // if (age > 18 && age < 21) {
    //
    //     console.log("Welcome to the club , don't drink!");
    //
    // } else if (age >= 21) {
    //
    //     console.log("Welcome to the club , don't get drunk!");
    //
    // } else {
    //
    //     console.log("Beat it kid!");
    //
    // }

//
// var input = prompt("What's your favorite fast food?");
//
// if (input === "Pizza" || input === "pizza") {
//
//     console.log("Cowabunga!");
//
// } else if (input === "Hamburger" || input === "hamburger" || input === "Burger" || input === "burger") {
//
//     console.log("Welcome to Good Burger!");
//
// } else if (input === "Wings" || input === "wings") {
//
//     console.log("Add some hot sauce into that!");
//
// } else {
//
//     console.log("Eww...!");
//
// }

//SWITCH STATEMENT NOTES - another method for conditional statements.

var color = prompt("Please enter a color:");

switch (color) {
    case "Red":
        alert("You chose Red!");
        break;
    case "Blue":
        alert("You chose Blue!");
        break;
    case "Green":
        alert("You chose Green!");
        break;
    default:
        alert("You did not enter a color");
        break;
}