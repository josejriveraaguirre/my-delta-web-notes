// MAP FILTER REDUCE EXERCISE
// Complete the following in a new js file name 'map-filter-reduce-exercise.js'
"use strict";
// const developers = [
//     {
//         name: 'stephen',
//         email: 'stephen@appddictionstudio.com',
//         languages: ['html', 'css', 'javascript', 'angular', 'php'],
//         yearExperience: 4
//     },
//     {
//         name: 'karen',
//         email: 'karen@appddictionstudio.com',
//         languages: ['java', 'javascript', 'spring boot'],
//         yearExperience: 3
//     },
//     {
//         name: 'juan',
//         email: 'juan@appddictionstudio.com',
//         languages: ['html', 'css', 'java', 'aws', 'php'],
//         yearExperience: 2
//     },
//     {
//         name: 'leslie',
//         email: 'leslie@codebound.com',
//         languages: ['node', 'npm', 'sql', 'javascript', 'java' ],
//         yearExperience: 5
//     },
//     {
//         name: 'dwight',
//         email: 'dwight@codebound.com',
//         languages: ['html', 'angular', 'javascript', 'sql'],
//         yearExperience: 8
//     }
// ];

/**Use .filter to create an array of developer objects where they have
 at least 5 languages in the languages array*/

// var devsFiltered = developers.filter
// (developer  =>  developer.languages.length >= 5
// );
// console.log(devsFiltered);

/**Use .map to create an array of strings where each element is a developer's
 email address*/

// var devsEmails = developers.map(developer => developer.email);
// console.log(devsEmails);

/**Use reduce to get the total years of experience from the list of developers.
 * Once you get the total of years you can use the result to calculate the average.*/

// var totYears = developers.reduce
// ((acc, developer) => acc + developer.yearExperience, 0);
// console.log(totYears);
//
// var avgYears = totYears/developers.length;
// console.log(avgYears);

/**Use reduce to get the longest email from the list.*/

// const longestEmail = developers.reduce((a, b) => a.email.length > b.email.length ? a : b).email;
// console.log(longestEmail);

/**Use reduce to get the list of developer's names in a single string
 - output:
 CodeBound Staff: stephen, karen, juan, leslie, dwight*/

// var names = developers.reduce((acc, developer) => acc + `${developer.name.charAt(0).toUpperCase() + developer.name.slice(1)}, `, "CodeBound Staff: ");
// console.log(names);

// BONUS
/** Use reduce to get the unique list of languages from the list
 of developers
 */

// const unique = developers.reduce((acc, dev))
//     dev.languages.map( lang => acc.push(lang));
//  acc.languages === lang.languages ? acc : lang, "");



// var acc = [];
// var langList = developers.reduce((acc, developer) => acc.push(developer.languages),"");
// console.log(langList);

// developers.reduce((unique, developer) => {
//     console.log(
//
//         developer.languages,
//
//         unique,
//
//         unique.includes(developer),
//
//         unique.includes(developer) ? unique : [...unique, developer],
//     );
//
//     return unique.includes(developer) ? unique : [...unique, developer];
// }, []);


 // const albums = [
 //     {artist: 'Michael Jackson', title: 'Thrillers', released: 1982, genre: 'Pop'},
 //     {artist: 'AC/DC', title: 'Back in Black', released: 1980, genre: 'Rock'},
 //     {artist: 'Pink Floyd', title: 'The Dark Side of the Moon', released: 1973, genre: 'Rock'},
 //     {artist: 'Bee Gees', title: 'Saturday Night Fever', released: 1977, genre: 'Disco'},
 //     {artist: 'Fleetwood Mac', title: 'Rumours', released: 1977, genre: 'Rock'},
 //     {artist: 'Shania Twain', title: 'Come On Over', released: 1997, genre: 'Country'},
 //     {artist: 'Michael Jackson', title: 'Bad', released: 1987, genre: 'Pop'},
 //     {artist: 'Led Zeppelin', title: 'Led Zeppelin IV', released: 1971, genre: 'Rock'},
 //     {artist: 'The Beatles', title: '1', released: 2000, genre: 'Rock'},
 //     {artist: 'Whitney Houston', title: 'Whitney', released: 1987, genre: 'Pop'},
 //     {artist: 'Def Leppard', title: 'Hysteria', released: 1987, genre: 'Rock'},
 //     {artist: 'Tupac', title: 'All Eyez on Me', released: 1996, genre: 'Rap'},
 //     {artist: 'Eminem', title: 'The Marshall Mathers LP', released: 2000, genre: 'Rap'},
 //     {artist: 'Green Day', title: 'Dookie', released: 1994, genre: 'Rock'},
 //     {artist: 'Michael Jackson', title: 'Dangerous', released: 1991, genre: 'Pop'},
 //     {artist: 'The Notorious B.I.G', title: 'Ready to Die', released: 1994, genre: 'Rap'},
 //     {artist: 'Adele', title: '21', released: 2011, genre: 'Pop'},
 //     {artist: 'Metallica', title: 'Load', released: 1996, genre: 'Rock'},
 //     {artist: 'Prince', title: '1999', released: 1982, genre: 'Pop'},
 //     {artist: 'Lady Gaga', title: 'Born This Way', released: 2011, genre: 'Pop'}];

 /** Create a filter function that logs every 'Pop' album*/

// var popAlbums = albums.filter((album)  =>  album.genre === "Pop");
// console.log(popAlbums);

/**  Create a filter function that logs every 'Rock' album*/

// var rockAlbums = albums.filter((album)  =>  album.genre === "Rock");
// console.log(rockAlbums);

/** Get the total value of years */




// const companies = [
//     {name: 'Walmart', category: 'Retail', start_year: 1962, location: { city: 'Rogers', state: 'AR' }},
//     {name: 'Microsoft', category: 'Technology', start_year: 1975, location: { city: 'Albuquerque', state: 'NM' }},
//     {name: 'Target', category: 'Retail', start_year: 1902, location: { city: 'Minneapolis', state: 'MN' }},
//     {name: 'Wells Fargo', category: 'Financial', start_year: 1852, location: { city: 'New York', state: 'NY' }},
//     {name: 'Amazon', category: 'Retail', start_year: 1994, location: { city: 'Bellevue', state: 'WA' }},
//     {name: 'Capital One', category: 'Financial', start_year: 1994, location: { city: 'Richmond', state: 'VA' }},
//     {name: 'CitiBank', category: 'Financial', start_year: 1812, location: { city: 'New York', state: 'NY' }},
//     {name: 'Apple', category: 'Technology', start_year: 1976, location: { city: 'Cupertino', state: 'CA' }},
//     {name: 'Best Buy', category: 'Retail', start_year: 1966, location: { city: 'Richfield', state: 'MN' }},
//     {name: 'Facebook', category: 'Technology', start_year: 2004, location: { city: 'Cambridge', state: 'MA' }},
// ];

// filter all of the company names

// var compName = companies.map(company => company.name);
// console.log(compName);

// filter all the company names started before 1990

// var before90s = companies.filter
// (company  =>  company.start_year < 1990
// );
// console.log(before90s);

// filter all the retail companies

// var retailComp = companies.filter(company => company.category === "Retail");
// console.log(retailComp);

// find all the technology and financial companies

// var techFin = companies.filter(company => company.category === "Technology" || company.category === "Financial");
// console.log(techFin);

// filter all the companies that are located in the state of NY

// var inNY = companies.filter(company => company.location.state === "NY");
// console.log(inNY);

// find all the companies that started on an even year.

// var evenYear = companies.filter(company => company.start_year % 2 === 0);
// console.log(evenYear);

// use map and multiply each start year by 2

// var yearBy2 = companies.map(company => company.start_year * 2);
// console.log(yearBy2);

// find the total of all the start year combined.

// var yearSum = companies.reduce((acc, company) => acc + company.start_year,0);
// console.log(yearSum);

// display all the company names in alphabetical order

// var alphaName = companies.map(company => company.name);
// console.log(alphaName.sort());

// display all the company names for youngest to oldest.

// const companyNamesList = companies.map(company => company.start_year);
// console.log(companyNamesList.sort());
//
// const reverseList = companyNamesList.reverse();
// console.log(reverseList);


// var youngToOld = [];
// for (companies.start_year of companies) {
//     var yearCheck = companies.reduce((a, b) => a.start_year > b.start_year ? a : b).name;
//     console.log(yearCheck);

// };

// console.log(youngToOld);