/* OBJECTS, LOOPS, FUNCTIONS... OH MY! */
// Complete the following in a new js file named objects-loops-functions.js
"use strict";
// const albums = [
//     {artist: 'Michael Jackson', title: 'Thrillers', released: 1982, genre: 'Pop'},
//     {artist: 'AC/DC', title: 'Back in Black', released: 1980, genre: 'Rock'},
//     {artist: 'Pink Floyd', title: 'The Dark Side of the Moon', released: 1973, genre: 'Rock'},
//     {artist: 'Bee Gees', title: 'Saturday Night Fever', released: 1977, genre: 'Disco'},
//     {artist: 'Fleetwood Mac', title: 'Rumours', released: 1977, genre: 'Rock'},
//     {artist: 'Shania Twain', title: 'Come On Over', released: 1997, genre: 'Country'},
//     {artist: 'Michael Jackson', title: 'Bad', released: 1987, genre: 'Pop'},
//     {artist: 'Led Zeppelin', title: 'Led Zeppelin IV', released: 1971, genre: 'Rock'},
//     {artist: 'The Beatles', title: '1', released: 2000, genre: 'Rock'},
//     {artist: 'Whitney Houston', title: 'Whitney', released: 1987, genre: 'Pop'},
//     {artist: 'Def Leppard', title: 'Hysteria', released: 1987, genre: 'Rock'},
//     {artist: 'Tupac', title: 'All Eyez on Me', released: 1996, genre: 'Rap'},
//     {artist: 'Eminem', title: 'The Marshall Mathers LP', released: 2000, genre: 'Rap'},
//     {artist: 'Green Day', title: 'Dookie', released: 1994, genre: 'Rock'},
//     {artist: 'Michael Jackson', title: 'Dangerous', released: 1991, genre: 'Pop'},
//     {artist: 'The Notorious B.I.G', title: 'Ready to Die', released: 1994, genre: 'Rap'},
//     {artist: 'Adele', title: '21', released: 2011, genre: 'Pop'},
//     {artist: 'Metallica', title: 'Load', released: 1996, genre: 'Rock'},
//     {artist: 'Prince', title: '1999', released: 1982, genre: 'Pop'},
//     {artist: 'Lady Gaga', title: 'Born This Way', released: 2011, genre: 'Pop'}
// ];
/**
 * 1. create a for loop function that logs every 'Pop' album
 * 2. create a for each function that logs every 'Rock' album
 * 3. create a for each function that logs every album released before 2000
 * 4. create a for loop function that logs every album between 1990 - 2020
 * 5. for loop function that logs every Michael Jackson album
 * 6. ? create a function name 'addAlbum' that accepts the same parameters from 'albums' and add it to the array
 */

// for ( var i = 0; i < albums.length; i++) {
//     switch (albums[i].genre === 'Pop') {
//         case true:
//             console.log(albums[i]);
//             break;
//     }
// }

// albums.forEach( function (album) {
//     switch (album.genre === 'Rock') {
//         case true:
//             console.log(album);
//             break;
//     }
// });

// albums.forEach( function (album) {
//     switch (album.released < 2000) {
//         case true:
//             console.log(album);
//             break;
//     }
// });

// for ( var i = 0; i < albums.length; i++) {
//     switch (albums[i].released >= 1990) {
//         case true:
//             switch(albums[i].released <= 2020) {
//                 case true:
//                     console.log(albums[i]);
//                     break;
//             }
//     }
// }

// for ( var i = 0; i < albums.length; i++) {
//     switch (albums[i].artist === 'Michael Jackson') {
//         case true:
//             console.log(albums[i]);
//             break;
//     }
// }


/**
 * TODO: Write a program that prints the numbers from 1 to 100.
 *  But for multiples of three print "Fizz" instead of the number
 *  and for the multiples of five print "Buzz".
 *  For numbers which are multiples of both three and five print "FizzBuzz".
 */

// for ( var i = 1; i <= 100; i++) {
//     switch (i % 3 === 0) {
//         case true:
//             switch (i % 5 === 0) {
//                 case true:
//                     console.log("FizzBuzz");
//                     continue;
//                 case false:
//                     console.log("Fizz");
//                     continue;
//             }
//         case false:
//             switch (i % 5 === 0) {
//                 case true:
//                     console.log("Buzz");
//                     continue;
//             }
//         }
//
//     console.log(i);
// }

/**
 * TODO: Write a function that takes in two parameters
 *  and returns the sum of both parameters, then multiplies it by 5.
 */

// function addMultFive(a,b) {
//
//     console.log(a+b);
//
//     console.log((a+b)*5);
//
// }
// addMultFive(2,3);

/* Create a function that prompts a user for their favorite day of the week and alerts a unique message based on the day.
Catch any invalid inputs (not indicating a day of the week).
For each day, allow the user to enter the abbreviated day (e.g. 'Monday', 'monday', 'Mon', or 'mon')
*/

// const weekdays = ['Monday', 'monday', 'Mon', 'mon', 'Tuesday', 'tuesday', 'Tue', 'tue', 'Wednesday', 'wednesday', 'Wed', 'wed', 'Thursday', 'thursday', 'Thu', 'thu', 'Friday', 'friday', 'Fri', 'fri', 'Saturday', 'saturday', 'Sat', 'sat', 'Sunday', 'sunday', 'Sun', 'sun']
//
//     do {
//
//         var input = prompt("Welcome! \nLet me inspire you. \nWhat's your favorite day of the week?");
//
//     } while (weekdays.includes (input) === false);
//
// function dailyQuote(input) {
//     if (input === "Monday" || input === "monday" || input === "Mon" || input === "mon") {
//         alert("'A goal is a dream with a deadline.'--Napoleon Hill");
//     }
//     if (input === "Tuesday" || input === "tuesday" || input === "Tue" || input === "tue") {
//         alert('"An entrepreneur is someone who jumps off a cliff and builds a plane on the way down."--Reid Hoffman');
//     }
//     if (input === "Wednesday" || input === "wednesday" || input === "Wed" || input === "wed") {
//         alert('"I don\'t look to jump over seven-foot bars. I look for one-foot bars that I can step over."--Warren Buffet');
//     }
//     if (input === "Thursday" || input === "thursday" || input === "Thu" || input === "thu") {
//         alert('"You can\'t have a million dollar dream with a minimum wage work ethic."--Stephen Hogan');
//     }
//     if (input === "Friday" || input === "friday" || input === "Fri" || input === "fri") {
//         alert('"The most dangerous poison is the feeling of achievement. The antidote is to, every evening, think what can be done better tomorrow."--Ingvar Kamprad');
//     }
//     if (input === "Saturday" || input === "saturday" || input === "Sat" || input === "sat") {
//         alert('"The true sign of intelligence is not knowledge but imagination."--Albert Einstein');
//     }
//     if (input === "Sunday" || input === "sunday" || input === "Sun" || input === "sun") {
//         alert('"The most courageous act is still to think for yourself. Aloud."--Coco Chanel');
//     }
// }
// dailyQuote(input);

/* Create a function that will return how many whitespace characters are at the beginning and end of a string. */

// from G WIP:
// var string = "   John Doe's iPhone6    ";
// console.log(string.split(" ").length-1);

// function whitespace(input) {
//     var length = input.length;
//     var trim = Number(input.trim().length);
//
//     console.log(Number(length - trim));
// }
//
// whitespace("   Jose     ");


