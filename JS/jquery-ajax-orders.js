"use strict";

/*
.done()
.fail()
.always()
 */

var ajaxRequest = $.ajax("data/orders.json");

ajaxRequest.done(function (data) {

    console.log(data);

    // console.log("First object in array " + data[0]);
    //
    // console.log(data[0].price);
    //
    // console.log(data[3].item);
    //
    // console.log(data[2].orderedBy);
    //
    // console.log(data[1].orderNum);

    var dataToHTML = buildHTML(data);

    $("#orders").html(dataToHTML);

});

function buildHTML(orders) {

    var orderHTML = "";

    //concatenation example

    // orders.forEach(function (order) {
    //
    //     orderHTML += "<section>";
    //     orderHTML += "<dl>";
    //     orderHTML += "<dt>" + "Ordered Item" + "</dt>";
    //     orderHTML += "<br>";
    //     orderHTML += "<dd>" + order.item + "</dd>";
    //     orderHTML += "</dl>";
    //     orderHTML += "</section>";
    //
    // });


    //template strings example
    orders.forEach(function (order) {

        orderHTML += `
        <h3>Ordered Item</h3>
        <p>${order.item}</p>
        <p>Ordered by: ${order.orderedBy}</p>
        <p>Total Amount: $${order.price}</p>
        `;

    });

    return orderHTML;

};