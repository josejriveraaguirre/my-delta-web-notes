// EVENT LISTENERS

/*
SYNTAX:
    target.addEventListener(type, listener, useCapture);
 */

/*
TYPE OF EVENTS
keyup
keydown
click
change
submit
mouseover
 */

var testButton = document.getElementById('testBtn');

//ADD EVENT LISTENER USING AN ANONYMOUS (= CALLBACK) FUNCTION

// testButton.addEventListener('click', function () {
//     if (document.body.style.backgroundColor === 'red') {
//         document.body.style.backgroundColor = 'white';
//     } else {
//         document.body.style.backgroundColor = 'red';
//     }
//
// });

//add even listener from a previously defined function

function toggleBackgroundColor() {
    if (document.body.style.backgroundColor === 'red') {
        document.body.style.backgroundColor = 'white';
    } else {
        document.body.style.backgroundColor = 'red';
    }
}

testButton.addEventListener('click',toggleBackgroundColor);

// REGISTER ADDITIONAL EVENTS

// at cursor over paragraph change color, font-family, font size

    function paragraphStyleChange(e) {
        e.target.style.color = 'green';
        e.target.style.fontFamily = 'Comic Sans Ms';
        e.target.style.fontSize = '30px';
    }

    var paragraph = document.getElementsByTagName('p');

    for (var i = 0; i < paragraph.length; i++) {
    paragraph[i].addEventListener('mouseover', paragraphStyleChange);
    }


// from Stephen:

function backgroundToBlue(e) {
    e.target.style.backgroundColor = 'blue';
}

var listItems1 = document.getElementsByTagName('li');

for (var i = 0; i < listItems1.length; i++) {
    listItems1[i].addEventListener('click', backgroundToBlue);
}

function myData(){ console.log("Hello, from CodeBound!");} myData();

// var code = function bound{}
// // var code = function bound() {}
// // function code(){}
// function(){}
// function function(a) { return a; }
// function getAnswer(a) { return a; }
// function getAnswer(a) { return function a; }