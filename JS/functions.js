
function count(input) {
    console.log(input.length);
}

count("Hello");

function add(a, b) {
    console.log(a + b);
}

add(6, 4);

function subtract(a, b) {
    console.log(a - b);
}

subtract(6, 4);

function multiply(a, b) {
    console.log(a * b);
}

multiply(6, 4);

function divide(numerator, denominator) {
    console.log(numerator / denominator);
}

divide(24, 6);

function remainder(number, divisor) {
    console.log(number % divisor);
}

remainder(25, 6);

function square(num) {
    console.log(num ** 2);
}

square(5);

var count = function (input) {
    console.log(input.length)
};

var result1= count("Hello");

var add = function (a, b) {
    console.log(a + b)
};

var result2= count("Hello");

var subtract = function (a, b) {
    console.log(a - b)
};

var result3 = subtract(6, 4);

var multiply = function (a, b) {
    console.log(a * b)
};

var result4= multiply(6, 4);

var divide = function (numerator, denominator) {
    console.log(numerator / denominator)
};

var result5 = divide(24, 6);

var remainder = function (number, divisor) {
    console.log(number % divisor)
};

var result6= remainder(25, 6);

var square = function (num) {
    console.log(num ** 2)
};

var result7 = square(5);