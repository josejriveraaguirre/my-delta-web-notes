/**
 * TODO: Change the body's background color to skyblue
 */

var body = document.getElementsByTagName('body');

body.item(0).style.backgroundColor = "skyblue";

//from WT:
// document.body.style.backgroundColor = "skyblue";

/**
 * TODO: Console log the section with an id of container
 */

var container = document.getElementById('container');

console.log(container);

//from WT:

console.log(document.getElementById('container'));

/**
 * TODO: Console log the section with an id of container using querySelector. What is a querySelector?
 */

var container = document.querySelector('#container');

console.log(container);

/**
 * TODO: Give the div with the class of header the text of "Hello World"
 */

var headerDiv = document.getElementsByClassName('header');

headerDiv.item(0).innerHTML = "<h1>Hello World!</h1>";

//from WT:

//document.getElementsByClassName('header').item(0).innerHTML = "Hello World!";

/**
 * TODO: Console log all of the list items with a class of "second"
 */

var listItems = document.getElementsByClassName('second');

console.log(listItems);

/**
 * TODO: Change the font family of all of the list items with a class of "second"
 */

var listItems = document.getElementsByClassName('second');

for (var i = 0; i < listItems.length; i++) {
    listItems.item(i).style.fontFamily = 'courier';
};

/**
 * TODO: Give the section with an id of container the text of "Go CodeBound!"
 */

var containerSec = document.getElementById('container');

containerSec.innerHTML += "<h1>Go CodeBound!</h1>";

/**
 * TODO: Create a new li element, give it the text "four", append it to the ul element
 */
var newItem =  '<li class="fourth">four</li>';

var uList = document.getElementsByTagName('ul');

uList.item(0).innerHTML += newItem;

/**
 * TODO: Give the div with a class of footer the text of "This is my Footer!"
 */

var footerDiv = document.getElementsByClassName('footer');

footerDiv.item(0).innerHTML = "<h1>This is my Footer!</h1>";

/**
 * TODO: BONUS:Have a message that says "Welcome to the DOM Exercise!" appear after 5 seconds.This message should only appear once.
 */

 var message = setTimeout(function () {
  alert("Welcome to the DOM Exercise!");
 }, 5000);
