/* JavaScript Loops
// Complete the following in a new js file named loops-exercise.js
*/
"use strict";
/** TODO: While Loops
 *   Create a while loop that uses console.log() to create the output shown below
 *   2
 *   4
 *   8
 *   16
 *   32
 *   64
 *   128
 *   256
 *   512
 *   1024
 *   2048
 *   4096
 *   8192
 *   16384
 *   32768
 *   65536
 */

// 1

// var num = 2;
// while (num <= 65536) {
//
//     console.log(num);
//
//     num *= 2;
// }

/** TODO: For Loops
 *   Create a function name showMultiplicationTable that accepts a number and console.logs the multiplication table for that number
 *   Example:
 *   showMultiplicationTable(8) should output
 *   8 x 1 = 8
 *   8 x 2 = 16
 *   8 x 3 = 24
 *   8 x 4 = 32
 *   8 x 5 = 40
 *   8 x 6 = 49
 *   8 x 7 = 56
 *   8 x 8 = 64
 *   8 x 9 = 72
 *   8 x 10 = 80
 */

// 2

// var num = prompt("Please enter a number:");
//
// for (var factor = 0; factor <= 12; factor++) {
//
//     var product = num * factor;
//
//     console.log(num + " x " + factor + " = " + product);
//
// }

// from WT:

// function showMultiplicationTable(input) {
//
//     for (var i = 1; i <= 10; i++) {
//
//         var answer = input * i;
//
//         console.log(`${input} x ${i} = ${answer}`);
//
//     }
//
// }
// showMultiplicationTable(input);

/** TODO: Create a for loop that uses console.log to create the output shown below.
 100
 95
 90
 85
 80
 75
 70
 65
 60
 55
 50
 45
 40
 35
 30
 25
 20
 15
 10
 5
 */

// 3

// for (var x = 100; x >= 5; x -= 5) {
//     console.log(x);
// }

/** TODO: Prompt the user for an odd number between 1 and 50.
 * Prompt the user for an odd number between 1 and 50.
 *
 *
 * Use a loop and a break statement to continue prompting
 *   the user if they enter invalid input.
 *
 *
 *
 *   Use a loop and the continue statement to output
 *   all the odd numbers between 1 and 50,
 *   except for the number the user entered.
 *   Output should look like this:
 Number to skip is: 27
 Here is an odd number: 1
 Here is an odd number: 3
 Here is an odd number: 5
 Here is an odd number: 7
 Here is an odd number: 9
 Here is an odd number: 11
 Here is an odd number: 13
 Here is an odd number: 15
 Here is an odd number: 17
 Here is an odd number: 19
 Here is an odd number: 21
 Here is an odd number: 23
 Here is an odd number: 25
 Yikes! Skipping number: 27
 Here is an odd number: 29
 Here is an odd number: 31
 Here is an odd number: 33
 Here is an odd number: 35
 Here is an odd number: 37
 Here is an odd number: 39
 Here is an odd number: 41
 Here is an odd number: 43
 Here is an odd number: 45
 Here is an odd number: 47
 Here is an odd number: 49
 */

// 4

// from WT:

// do {
//
//     var userInput = prompt("Enter and odd number between 1-50");
//
//     if (userInput % 2!== 0 && userInput >= 1 && userInput <= 50) {
//
//         break;
//
//     }
// } while(true);
//
// for (var i = 1; i <= 50; i += 2) {
//
//    if (i=== userInput) {
//
//        console.log("Yikes! Skipping Number: " +userInput);
//        continue;
//
//    }
//     console.log("here is an odd number: " + i);
// }

// 4

//
// // for (var check= false; check = true;check) {
// //
// //     console.log(check);
//
//     var input = prompt("Please enter an odd number between 1 and 50:")
//
// //     console.log(input);
// //
// //     if (input < 1 || input > 49) {
// //
// //         check = "false";
// //     }
// //     else
// //
// //         check = "true";
// //
// //         break;
// // }
//
//     console.log("Number to skip is: " + input);
//
//     for (var oddNum = 1; oddNum <= 50; oddNum += 2) {
//
//         if (oddNum === Number(input)) {
//
//             console.log("Yikes! Skipping number: " + oddNum);
//
//             continue;
//         }
//
//         console.log("Here is an odd number: " + oddNum);
//     }


/** TODO:
 *  Write a for loop that will iterate from 0 to 20. For each iteration, it will check if the current number is even or odd,
 *  and report that to the screen (e.g. "2 is even").
 */

// 5

// var class = "";
// for (var num = 1;num <= 20;num++) {
//
//     if (num % 2 == 0) {
//         case true:
//         class = "even";
//     }
//     console.log(num + " is " + ".")
//
// }

// from WT:

// for (var i = 1; i<= 20; i++) {
//
//     if (i % 2 === 0) {
//         console.log(i + " is even.");
//     }
//     else {
//         console.log(i + " is odd.");
//     }
// }

/**
 * TODO:
 *  Write a for loop that will iterate from 0 to 10. For each iteration of the for loop,
 *  it will multiply the number by 9 and log the result (e.g. "2 * 9 = 18").
 */

// from WT:

// for (var i = 0; i <= 10; i++) {
//
//     console.log(i + " * 9 = " + (i * 9));
//
// }

// from

/** BONUS:
 * Write a program that finds the summation of every number from 1 to num.
 * The number will always be a positive integer greater than 0.
 *
 * Ex.
 * summation(2) -> 3
 * 1 + 2
 *
 * summation(8) -> 36
 * 1 + 2 + 3 + 4 + 5 + 6 + 7 + 8
 */

function summation(num) {

    var total = 0;

    for (var i = 1; i <= num; i++) {

        total += i;
    }

    console.log(total);
}

summation(2);

/** BONUS:
 * Create a for loop that uses console.log to create the output shown below.
 *
 * 1
 * 22
 * 333
 * 4444
 * 55555
 * 666666
 * 7777777
 * 88888888
 * 999999999
 */

for (var i = 1; i <= 9; i++) {
    i = i.toString();
    console.log(i.repeat(i));
}

var output = "";

for (var i = 1; i <= 9; i++) {

    output = "";

    for (var j = 1; j<=i; j++) {

    output += i;

    }

    console.log(output);

}