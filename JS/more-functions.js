
//Global variables are outside the function
//Local variables are inside the function
//1
function isOdd(x) {
console.log(x % 2!=0);
}
isOdd(5);

//2
function isEven(x) {
    console.log(x % 2==0);
}
isEven(5);

//3
function isSame(input) {
    var match = "password";
    console.log(input===match);
}
isSame("password");

//4
// function isSeven(input) {
//     var match = 7;
//     console.log(input===match);
// }
// isSeven(8);

//4-Stephen's correction
function isSeven(x) {
    console.log(x === 7);
}
isSeven(8);

//5
function addTwo(x) {
    console.log(x + 2);
}
addTwo(5);

//6
function isMultipleOfTen(x) {
    console.log(x % 10===0);
}
isMultipleOfTen(51);

//7
function isMultipleOfTwo(x) {
    console.log(x % 2===0);
}
isMultipleOfTwo(50);

//Stephens comment about RETURN vs CONSOLE LOG

// function isMultipleOfTwo(x) {
//     return x % 2===0;
// }
// console.log(isMultipleOfTwo(50));


//8
function isMultipleOfTwoAndFour(x) {
    console.log(x % 2===0 && x % 4===0);
}
isMultipleOfTwoAndFour(48);

//9
function isTrue(input) {
    console.log(input==="true");
}
isTrue("true");

//10
function isFalse(input) {
    console.log(input==="false");
}
isFalse("false");

//11
function isVowel(input) {
    console.log(input==="a" || input==="e" || input==="i" || input==="o" || input==="u"
        || input==="A" || input==="E" || input==="I" || input==="O" || input==="U");
}
isVowel("E");

//12
function triple(x) {
    console.log(x * 3);
}
triple(5);

//13
function quad(x) {
    console.log(x * 4);
}
quad(5);

//14
function degreesToRadians(x) {
    console.log(x * Math.PI/180);
}
degreesToRadians(45);


//15
function ReverseString(str) {

    // Check input
    if(!str || str.length < 2 ||
        typeof str!== 'string') {
        return 'Not valid';
    }

    // Take empty array revArray
    const revArray = [];
    const length = str.length - 1;

    // Looping from the end
    for(let i = length; i >= 0; i--) {
        revArray.push(str[i]);
    }

    // Joining the array elements
    return revArray.join('');
}

console.log(ReverseString("CodeBound"));

// alternate solution from Stephen in my opinion this is a more to
// the point solution and better for us to understand at this level.

function reverseString(input) {
    var splitString = input.split("");
    var reverseOrder = splitString.reverse();
    var reverseWord = reverseOrder.join("");
    return reverseWord;
}
console.log(reverseString("Delta"));

//Feb 8 review

function triple(number) {
    return number * 3;
}