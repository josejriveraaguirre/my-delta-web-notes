"use strict";

//jQuery Document Ready

$(document).ready(function () {
    alert("This page has finished loading.");
});

// Javascript

// window.onload = function () {
//     alert("This page has finished loading.");
// }

// JQuery selectors
// syntax: $("selector")

// ID selector

var content = $("#codebound").html(); // similar to .innerHTML
console.log(content);

// CSS selector .css() similar to the .style property

$(".urgent").css("background-color", "red"); // single property

$(".not-urgent").css(
    {
        "background-color": "yellow",
        "text-decoration": "line-through"
    }
);

// multiple style properties

// multiple selector
$(".urgent, p, #codebound").css("color", "orange");

// all selector

// $("*").css("background-color", "green");

// element selector
$("h1").css("text-decoration", "underline");