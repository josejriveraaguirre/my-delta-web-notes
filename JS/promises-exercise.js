(function () {

    "use strict";

// CREATE A FILE NAMED 'promises-exercise.js' INSIDE OF YOUR JS DIRECTORY

// Exercise 1:
// Write a function name 'wait' that accepts a number as a parameter, and returns
// a PROMISE that RESOLVES AFTER THE PASSED NUMBER OF MILLISECONDS
    // EX. wait(4000).then(() => console.log('You will see this after 4 seconds.'));
    // EX. wait(8000).then(() => console.log('You will see this after 8 seconds.'));
    //
    // var n = 5;
    //
    // var wait = new Promise(function (resolve, reject) {
    //
    //     setTimeout(function () {
    //
    //         resolve(n);
    //
    //     }, n*1000);
    //
    // });
    //
    // wait.then(function (en) {
    //
    //     console.log(`You are seeing this after ${n} seconds.`);
    //
    // });

// Exercise 2:
// Write a function testNum that takes a number as an argument and returns a Promise that tests if the value is less than or greater than the value 10.

//     var x = 10;
//
//     var testNum = new Promise(function (resolve, reject) {
//
//         if (x < 10) {
//
//             resolve(x);
//
//         }
//         else {
//
//             reject(x);
//
//         }
//
//     });
//
// testNum.then(function (ex) {
//
//     console.log(`${x} is less than 10.`);
//
// }).catch(function () {
//
//     var testNum1 = new Promise(function (resolve, reject) {
//
//         if (x > 10) {
//
//             resolve(x);
//
//         }
//         else {
//
//             reject(x);
//
//         }
//
//     });
//
//     testNum1.then(function (ex) {
//
//         console.log(`${x} is greater than 10.`);
//
// }).catch(function () {
//
// console.log(`${x} is equal to 10.`);
//
// })
//
// });


// Exercise 3:
    //Write two functions that use Promises that you can chain! The first function, makeAllCaps(), will take in an array of words and capitalize them, and then the second function, sortWords(), will sort the words in alphabetical order. If the array contains anything but strings, it should throw an error.

    var days = [10, "monday", "tuesday", "wednesday", "thursday", "friday", "saturday"];

    console.log(days);

    var makeAllCapsAndSortWords = new Promise(function (resolve, reject) {

        resolve(days);


        reject(days);

    });

    makeAllCapsAndSortWords.then(function(days) {

        var capDays = days.map(function(day) {
            return day.toUpperCase();
        })
        console.log(capDays);

        var sortDays = capDays.sort();
        console.log(sortDays);

    }).catch(function(days) {

    console.log(`ERROR: Non string element in array "days"!`);

    });

})()