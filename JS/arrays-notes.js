// JAVASCRIPT ARRAYS

// BASIC SYNTAX
// [] - empty array

var myArray = ["red", 12, -144, true];

// console.log(myArray);
//
// console.log(myArray.length);
//
// console.log(myArray[2]);
//
// console.log(myArray[4]);

//ITERATING ARRAY

var delta = ["jose", "alyssa", "victor", "angela"];

// console.log(`There are ${delta.length} members in Delta Cohort.`)

//LOOP THROUGH THE ARRAY

for (var i = 0; i < delta.length; i++) {

// console.log(delta[i]);

}

// forEach loop

/*syntax:
// nameOfVariable.forEach( function ( element, index, array ) {

 code to run
 });

*/
//
// var ratings = [25, 34, 57, 62, 78, 89,91];
//
// ratings.forEach(function (rating) {
//
//     console.log(rating);
//
// });
//
// var pizza = ["cheese", "dough", "sauce", "pineapple", "canadian bacon"];
//
// pizza.forEach(function (ingredient) {
//
//     console.log(ingredient);
//
// });

//CHANGING / MANIPULATING ARRAYS

var fruits = ["apple", "grape", "strawberry", "banana"];

console.log(fruits);
//
// fruits.push("watermelon", "cherry", "mango", "pineapple");
//
// // console.log(fruits);
//
// fruits.unshift("dragon fruit");

// console.log(fruits);

// fruits.pop();
//
// fruits.shift();
//
// console.log(fruits);

// LOCATING ARRAY ELEMENT(S)
var indexElement = fruits.indexOf("strawberry");
// console.log(indexElement);

//SLICING
// .slice() - copies part of it.

var slice = fruits.slice(1, 3);
//
// console.log(slice);
// console.log(fruits);

//REVERSE

fruits.reverse();
console.log(fruits);

//sort

console.log(fruits.sort());
