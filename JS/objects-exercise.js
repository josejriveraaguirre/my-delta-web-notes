/* JAVASCRIPT OBJECTS */
// Complete the following in a new js file named objects-exercise.js
"use strict";
/**
 * TODO: Create an object called movie. Include four properties
 *  Console log each property.
 */

// const movies = [
//
//     {
//         title: 'DR. NO',
//         released: 1962,
//         actor: 'Sean Connery',
//         co_star: 'Ursula Andress'
//     },
//     {
//         title: 'FROM RUSSIA WITH LOVE',
//         released: 1963,
//         actor: 'Sean Connery',
//         co_star: 'Daniela Bianchi'
//     },
//     {
//         title: 'GOLDFINGER',
//         released: 1964,
//         actor: 'Sean Connery',
//         co_star: 'Honor Blackman'
//     },
//     {
//         title: 'THUNDERBALL',
//         released: 1965,
//         actor: 'Sean Connery',
//         co_star: 'Claudine Auger'
//     },
//     {
//         title: 'YOU ONLY LIVE TWICE',
//         released: 1967,
//         actor: 'Sean Connery',
//         co_star: 'Akiko Wakabayashi'
//     },
//     {
//         title: 'ON HER MAJESTY\'S SECRET SERVICE',
//         released: 1969,
//         actor: 'George Lazenby',
//         co_star: 'Diana Rigg'
//     },
//     {
//         title: 'DIAMONDS ARE FOREVER',
//         released: 1971,
//         actor: 'Sean Connery',
//         co_star: 'Jill St. John'
//     },
//     {
//         title: 'LIVE AND LET DIE',
//         released: 1973,
//         actor: 'Roger Moore',
//         co_star: 'Jane Seymour'
//     },
//     {
//         title: 'THE MAN WITH THE GOLDEN GUN',
//         released: 1974,
//         actor: 'Roger Moore',
//         co_star: 'Britt Ekland'
//     }
// ];
// for (var i = 0; i < movies.length; i++) {
//
//     console.log(movies[i].title);
//
//     console.log(movies[i].released);
//
//     console.log(movies[i].actor);
//
//     console.log(movies[i].co_star);
//
// }

/**
 * TODO: Create an object called movie include four properties of title, protagonist, antagonist, genre
 *  console log each property.
 */

// const movie = {
//         title: 'DIAMONDS ARE FOREVER',
//         protagonist: 'Sean Connery',
//         antagonist: 'Blofeld',
//         genre: 'Action'
//     }
//
//     console.log('One of my favorite movies:')
//
//     console.log('Title: ' + movie.title);
//
//     console.log('Protagonist: ' + movie.protagonist);
//
//     console.log('Antagonist: ' + movie.antagonist);
//
//     console.log('Genre: ' + movie.genre);


/**
 * TODO: Create a pet/companion object include the following properties:
 *  string name, number age, string species, number valueInDollars,
 *  array nickNames, vaccinations: date, type, description.
 */

// const pet_companion = [
//
//     {
//         name: 'Fido',
//         age: 2,
//         species: 'dog',
//         valueInDollars: 100.00,
//         nicknames: ['pup', 'beast', 'furball'],
//         vaccinations: {
//             date: '01/2021',
//             type: 'ringworm',
//             description: 'to prevent ringworm'
//         }
//     }
// ];
//
// console.log(pet_companion[0].name);
// console.log(pet_companion[0].age);
// console.log(pet_companion[0].species);
// console.log("$" + pet_companion[0].valueInDollars);
// console.log(pet_companion[0].nicknames[2]);
// console.log(pet_companion[0].vaccinations.date);
// console.log(pet_companion[0].vaccinations.type);
// console.log(pet_companion[0].vaccinations.description);


/**
 * TODO: Add 3 additional objects to the array below called movies. Be sure to include five properties: title, releaseDate, director, genre, and myReview.
 *  The myReview property should be a function
 */
// EX

// const movies = [
//     {
//         title: 'Batman',
//         releaseDate: {
//             month: 'June',
//             date: 23,
//             year: 1989
//         },
//         director: {
//             firstName: 'Tim',
//             lastName: 'Burton'
//         },
//         genre: ['Action', 'Suspense', 'Superhero'],
//         myReview: function () {
//             console.log('This is Batman! Michael Keaton is Batman! 4/4 stars!');
//         }
//     },
//     {
//         title: 'Superman',
//         releaseDate: {
//             month: 'July',
//             date: 24,
//             year: 1990
//         },
//         director: {
//             firstName: 'Franco',
//             lastName: 'Zeffirelli'
//         },
//         genre: ['Action', 'Suspense', 'Superhero'],
//         myReview: function () {
//             console.log('This is Superman! Christopher Reeve is Superman! 3/4 stars!');
//         }
//     },
//     {
//         title: 'Aquaman',
//         releaseDate: {
//             month: 'August',
//             date: 25,
//             year: 1991
//         },
//         director: {
//             firstName: 'Dino',
//             lastName: 'De Laurentiis'
//         },
//         genre: ['Action', 'Suspense', 'Superhero'],
//         myReview: function () {
//             console.log('This is Aquaman! Jason Momoa is Aquaman! 2/4 stars!');
//         }
//     },
//     {
//         title: 'Ironman',
//         releaseDate: {
//             month: 'June',
//             date: 26,
//             year: 1992
//         },
//         director: {
//             firstName: 'Martin',
//             lastName: 'Scorsese'
//         },
//         genre: ['Action', 'Suspense', 'Superhero'],
//         myReview: function () {
//             console.log('This is Ironman! Robert Downey Jr. is Ironman! 1/4 stars!');
//         }
//     }
// ];

// movies[0].myReview();

/** Once you added the additional movies, complete the following:
 *  Loop through the array to display and console log the 'Release Date: month / date / year'.
 *
 *  Loop through the array to display and console log the 'Title, ReleaseDate, Director, myReview'
 *
 * Loop through the array to display and console log the 'Genre'
 *
 *  Loop through the array to display and console log the 'Director: FirstName LastName'
 */

// for (var i = 0; i < movies.length; i++) {
//
//     console.log(movies[i].title);
//
//     console.log('Release Date: ${movies[i].releaseDate.month} / ${movies[i].releaseDate.date} / ${movies[i].releaseDate.year}');
//
// }

// for (var i = 0; i < movies.length; i++) {
//
//     console.log(movies[i].title);
//
//     console.log(movies[i].releaseDate);
//
//     console.log(movies[i].director);
//
//     console.log(movies[i].myReview());
//
// }

// for (var i = 0; i < movies.length; i++) {
//
//     console.log(movies[i].title);
//
//     console.log(movies[i].genre);
//
// }

// for (var i = 0; i < movies.length; i++) {
//
//     console.log(movies[i].title);
//
//     console.log(movies[i].director);
//
// }

// BONUS
/** TODO:
 * HEB has an offer for the shoppers that buy products amounting to
 * more than $200. If a shopper spends more than $200, they get a 12%
 * discount. Write a JS program, using conditionals, that logs to the
 * browser, how much Stephen, Justin and Leslie need to pay. We know that
 * Justin bought $180, Stephen $250 and Leslie $320. Your program will have to
 * display a line with the name of the person, the amount before the
 * discount, the discount, if any, and the amount after the discount.
 *
 * Uncomment the lines below to create an array of objects where each object
 * represents one shopper. Use a foreach loop to iterate through the array,
 * and console.log the relevant messages for each person
 */

// var shoppers = [
//         {
//             name: 'Justin',
//             amount: 180
//         },
//         {
//             name: 'Stephen',
//             amount: 250
//         },
//         {
//             name: 'Leslie',
//             amount: 320
//         }
// ];
// for (var i = 0; i < shoppers.length; i++) {
//
//     switch (shoppers[i].amount > 200) {
//
//         case true:
//             console.log("Shopper's name: " + shoppers[i].name);
//             console.log("Purchase Amount: $" + shoppers[i].amount);
//             console.log("");
//             console.log("SPECIAL DISCOUNT ON PURCHASES OVER $200:\nYou saved: $" + (shoppers[i].amount * .12) + " today!");
//             console.log("");
//             console.log("Please pay : $" + (shoppers[i].amount * .88));
//             console.log("Thank you for visiting us today!");
//             console.log(".............................................");
//             console.log("");
//             break;
//
//         case false:
//             console.log("Shopper's name: " + shoppers[i].name);
//             console.log("Please pay : $" + (shoppers[i].amount));
//             console.log("Thank you for visiting us today!");
//             console.log(".............................................");
//             console.log("");
//             break;
//
//     }
//
// }
