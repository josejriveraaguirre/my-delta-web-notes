// get the main element by id
var mainHeader = document.getElementById('main-header');
console.log(mainHeader);
console.log(mainHeader.innerHTML);

//set innert html of mainHeader to "Winter is gone"

mainHeader.innerHTML = "Winter is Gone!";
console.log(mainHeader);
console.log(mainHeader.innerHTML);

// assign a variable named 'subHeader' to the sub header element by id

var subHeader = document.getElementById('sub-header');
console.log(subHeader);
console.log(subHeader.innerHTML);

subHeader.innerHTML = "Winter please stay gone until next year, and spare us the snow!";
console.log(subHeader);
console.log(subHeader.innerHTML);

subHeader.style.color = "blue";

//TODO: assign a variable named 'listItems' to all list items

var listItems = document.getElementsByTagName("li");

// TODO: set the color on EVERY OTHER list item to grey
// TODO: set text color of li with data-dbid = 1 to blue

for (var i = 0; i < listItems.length; i++) {

    if (i % 2 !== 0) {
        listItems[i].style.color = 'red';
    }

    if (listItems[i].hasAttribute('data-dbid')) {
        if (listItems[i].getAttribute('data-dbid') ==='1') {
            listItems[i].style.color = 'blue';
        }

    }

}

var subParagraph = document.getElementsByClassName("sub-paragraph");
console.log(subParagraph);
console.log(subParagraph.innerHTML);

subParagraph.item(0).innerHTML = "New text for the sub-paragraph.";

console.log(subParagraph);
console.log(subParagraph.innerHTML);

var planetsString = "Mercury|Venus|Earth|Mars|Jupiter|Saturn|Uranus|Neptune";
var planetsArray = planetsString.split("|");
console.log(planetsArray);
console.log(planetsString)

var html = "<ul><li>";
html += planetsArray.join("</li><li>");
html += "</li></ul>";

var planetsList = document.getElementById("planets");
planetsList.innerHTML = html;




