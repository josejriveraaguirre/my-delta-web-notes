/* JAVASCRIPT DOM */
// NOTE: You'll need to create an html to link this js file, along with creating some content in the html
"use strict";
/** When the button with the id of `change-bg-color` is clicked the background of
 the page should turn blue.*/

// var changeBgButton = document.getElementById('change-bg-color');
//
// function toggleBackgroundColor() {
//
//     if (document.body.style.backgroundColor === 'blue') {
//         document.body.style.backgroundColor = 'white';
//     } else {
//         document.body.style.backgroundColor = 'blue';
//     }
// }
//
// changeBgButton.addEventListener('click',toggleBackgroundColor);


/** When the button with an id of `append-to-ul` is clicked, append an li with
 the content of `text` to the ul with the id of `append-to-me`.*/

// WIP:

// var appendButton = document.getElementById('append-to-ul');
//
//     function appendListItem() {
//
//         var uList = document.getElementById('append-to-me');
//
//         var newItem = '<li>Item Eleven: text</li>';
//
//         ulist.innerHTML += newItem;
//
// }
//
// appendButton.addEventListener('click', appendListItem);

/** Ten seconds after the page loads, the heading with the id of `message` should
 change it's text to "Goodbye, World!". Completed*/

// var byeMessage = setTimeout(function () {
//     document.getElementById('message').innerHTML = "Goodbye, World!";
// }, 10000);

/** When a list item inside of the ul with the id of `hl-toggle` is first
 clicked, the background of the li that was clicked should change to
 yellow.
 When a list item that has a yellow background is clicked, the
 background should change back to the original background.
 */

// var hlItem = document.getElementById('hl-toggle');
//
// function highlightItem() {
//
//     if (hlItem.style.backgroundColor === 'yellow') {
//
//         hlItem.style.backgroundColor = 'white';
//
//     } else {
//
//         hlItem.style.backgroundColor = 'yellow';
//
//     }
//
// }
//
// hlItem.addEventListener('click',highlightItem);

// function backgroundToBlue(e) {
//     e.target.style.backgroundColor = 'blue';
// }
//
// var listItems1 = document.getElementsByTagName('li');

// for (var i = 0; i < listItems1.length; i++) {
//     listItems1[i].addEventListener('click', backgroundToBlue);
// }

/** When the button with the id of `upcase-name` is clicked, the element with the
 id of `output` should display the text "Your name uppercased is: " + the
 value of the `input` element with the id of `input` transformed to uppercase.*/

var uppBtn = document.getElementById('upcase-name');
var upperName = document.getElementById('output');
var lowerName = document.getElementById('input');

function uppCaseName() {

    var converted = lowerName.value.toUpperCase();

    upperName.innerHTML = converted;

}

uppBtn.addEventListener('click',uppCaseName);


/** Whenever a list item inside of the ul with the id of `font-grow` is _double_
 clicked, the font size of the list item that was clicked should double.*/

var growItem = document.getElementById('font-grow');

function doubleSize() {


        growItem.style.fontSize = '200%';

    }

growItem.addEventListener('dblclick',doubleSize);
