"use strict";

var numbers = [11, 20, 33, 40, 55, 60, 77, 80, 99, 100];

var evens = [];
var odds = [];

for (var i = 0; i < numbers.length; i++) {

    if(numbers[i] % 2 === 0) {
        evens.push(numbers[i]);
    } else {
        odds.push(numbers[i]);
    }
};

// console.log(evens);
// console.log(odds);

var evensFiltered = numbers.filter(function(n) {
    return n % 2 === 0;
});
// console.log(evens);

var oddsFiltered = numbers.filter(function(n) {
    return n % 2 !== 0;
});
// console.log(odds)

// var numbersTimesTen = numbers.map(function (num) {
// return num * 10;
// });
// console.log(numbersTimesTen);

const numbersTimesTen = numbers.map(num => num * 10);

// console.log(numbersTimesTen);

const myArray = [1, 2, 3, 4, 5, ]

// const sum = myArray.reduce(function(acc, currentNumber) {
//     return acc + currentNumber;
// }, 0);
//
// console.log(sum);

const sum = myArray.reduce((acc, currentNumber) =>  acc + currentNumber, 0);

// console.log(sum);

const officeSales = [

    {
        name: "Jim Halpert",
        sales: 500

    },
    {
        name: "Dwight Schrute",
        sales: 750

    },
    {
        name: "Ryan Howard",
        sales: 150

    }

];

const money = officeSales.reduce((acc, person) => acc + person.sales, 0);
console.log(money);