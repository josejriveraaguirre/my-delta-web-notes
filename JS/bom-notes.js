 // setInterval, clearInterval, setTimeout, clearTimeout

 // Reload the page after 8000ms

//  var stop = setTimeout(function () {
//      location.reload();
//  }, 8000);
//
// clearTimeout(stop);

var i = 0;
var callback = function () {
    i++;
    console.log("Hooray!!! "+ i);
}

// var timeout = setTimeout(callback, 4000);
//
// clearTimeout(timeout);
//
// // setInterval example
//
// // setInterval(callback, 4000);
//
// var timeout2 = setInterval(callback, 2000);
//
// setTimeout(function () {
//
//     clearInterval(timeout2);
//
// }, 10000);

// use this to navigate to another page

// window.location = "https://www.netflix.com";

var netflix = setTimeout(function () {
    window.location = "https://www.netflix.com";
}, 5000);