// Javascript objects
// var car = {
//     make: "Toyota",
//     model: "4Runner",
//     color: "Hunter Green",
//     numberOfWheels: 5
// }
//
// console.log(car);
// console.log(car.color);

//nested values

var cars = [
    {
        make: "Toyota",
        model: "4Runner",
        color: "Hunter Green",
        numberOfWheels: 5,
        features: ["Heated Seats", "Bluetooth Radio", "Automatic Doors"],
        alarm: function () {
            alert("Sound the alarm!");
        }
    },
    {
        make: "Honda",
        model: "Civic",
        color: "Black",
        numberOfWheels: 4,
        features: ["Great gas mileage", "FM/AM Radio", "Still runs"],
        alarm: function () {
            alert("No alarm, sorry");
        },
        owner: {
            name: "John Doe",
            age: 35
        }
    },
    {
        make: "Ford",
        model: "Bronco",
        color: "Navy Blue",
        numberOfWheels: 4,
        features: ["Power windows", "Bluetooth Radio", "GPS navigation"],
        alarm: function () {
            alert("Move, get out of the way!");
        },

        owner: {
            name: "Jane Doe",
            age: 30,
            address: {
                street: "123 Oak Dr.",
                city: "San Antonio",
                state: "TX"
            }
        }
    }
];

// console.log(cars[2].owner.address.city);
// console.log(cars[1].alarm());

//
// cars.forEach(function (car) {
//     car.features.forEach(function(feature){
//         console.log(feature);
//     })
//
// });

// 'this' keyword

var videoGames = {};

videoGames.name = "Super Mario";

videoGames.company = "Nintendo";

videoGames.genre = "Adventure";

videoGames.description =

function () {

    alert("Video Game Name: " + this.name

    + "\nCompany of Video Game: " + this.company

        + "\nGenre of Video Game: " + this.genre);

};

videoGames.description();
