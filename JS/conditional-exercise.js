"use strict";
/**
 * TODO:
 * Write some JavaScript that uses a 'confirm' dialog to ask the user if they would like to enter a number. If they click 'Ok', prompt the user for a number, then use 3 separate alerts to tell the user:
 * - Whether the number is even or odd
 * - What the number plus 100 is
 * - If the number is negative or positive
 * If the user doesn't enter a number, use an alert to tell them that, and do not display any of the information above
 **/

// var input = confirm("Shall we play a game?");
//
//     if (input == false) {
//
//     alert("Maybe next time...");
//
//     } else {
//
//     var num = prompt("Let's start by entering a number... please:");
//
//     }
//
// // from WT: isNaN(input)
//
//     if ((num == null || num == "" )&& input == true) {
//
//         alert("You did not enter a number. I know you can do this. Please refresh the page to try again.");
//
//     }
//
//         if ( input==true) {
//
//         var result1 = num % 2;
//
//         }
//
//         if (num !== "" && result1 === 0) {
//
// // from WT: add the var value(s) to the alert(s) later.
//
//             alert("That number is even.");
//
//         }
//
//         if (num !== "" && result1 !== 0) {
//
//         alert("That number is odd.");
//
//         }
//
//         if (num !== "") {
//
//         var result2 = Number(num);
//
//         function addOneHundred(x) {
//
//         alert("That number plus a hundred = " + (x + 100));
//
//         }
//
//         addOneHundred(result2);
//
//         }
//
//         if (num !== "" && num < 0) {
//
//         alert("That number is negative.");
//
//         }
//
//         if (num !== "" && num >= 0) {
//
//         alert("That number is positive.");
//
//         }


/**
 * TODO:
 * Create a function named 'color' that accepts a string that is a color name as an input. This function should return a message that related to that color. Only worry about the colors defined below, if the color passed is not one of the ones defined below, return a message that says so.
 * Ex
 * color('blue') // returns "blue is the color of the sky"
 * color('red') // returns "Roses are red"
 * You should use an if-else if- else block to return different messages.
 */

// var color = prompt("Guess what color I'm thinking of, and enter it here:");
//
//     function colorVerse (color) {
//
//         if (color === "blue") {
//
//             alert("Blue is the color of the sky.");
//
//         } else if (color === "red") {
//
//             alert("Roses are red.");
//
//         } else if (color !== "red" && color !== "blue") {
//
//             alert("Guess again!");
//
//         }
//
//     }
//
//     colorVerse(color);

/**
 * TODO:
 * It's the year 2021, it's fall, and it's safe to shop again!
 * Suppose there's a promotion at Target, each customer is given a randomly generated "lucky Target number" between 0 and 5. If your lucky number is 0, you have no discount, if your lucky number is 1 you'll get a 10% discount, 2 is a 25% discount, 3 is a 35% discount, 4 is a 50% discount, and 5 you'll get a 75% discount.
 * Write a function named 'calculateTotal' that accepts a lucky number and total amount, and returns the discounted price.
 * Ex
 * calculateTotal(0, 100) // returns 100
 * calculateTotal(4, 100) // return 50
 * Test your function by passing it various values and checking for the expected return value.
 */

// var luckyNum = Number(prompt("Let's start by entering your lucky number (between 0 and 5), please:"));
// var totAmt = Number(prompt("Now please enter the total amount of your purchase:"));
// var discdAmt =0;
//
//     if (luckyNum === 0) {
//
//         var discdAmt = 1
//
//     } else if (luckyNum === 1) {
//
//         var discdAmt = .90
//
//     } else if (luckyNum === 2) {
//
//         var discdAmt = .75
//
//     } else if (luckyNum === 3) {
//
//         var discdAmt = .65
//
//     } else if (luckyNum === 4) {
//
//         var discdAmt = .50
//
//     } else if (luckyNum === 5) {
//
//         var discdAmt = .25
//
//     }
//         function calculateTotal(totAmt, discdAmt) {
//
//             alert("Your only pay: $" + (totAmt * discdAmt));
//         }
//         calculateTotal(totAmt, discdAmt);
//
// // from WT: Stephen's solution is more economical.

/**
 * TODO:
 * Create a grading system using if/else if/ else statements
 // BONUS Use the switch statements
 // 100 - 97 A+
 // 96 - 94 A
 // 93 - 90 A -
 // 89 - 87 B +
 // 86 - 84 B
 // 83 - 80 B -
 // 79 - 77 C +
 // 76 - 74 C
 // 73 - 70 C -
 // 69 - 67 D +
 // 66 - 64 D
 // 63 - 60 D -
 // < 59 = F
 */
// // from WT:
// var grade= prompt("Enter your grade:");
//
//     if (grade >= 97) {
//         alert("Grade: A+");
//     }
//     else if (grade >= 94 && grade <= 96) {
//         alert("Grade: A");
//     }
//     else if (grade >= 90 && grade <= 93) {
//         alert("Grade: A-");
//     }
//     else if (grade >= 87 && grade <= 89) {
//         alert("Grade: B+");
//     }
//     else if (grade >= 84 && grade <= 86) {
//         alert("Grade: B");
//     }
//     else if (grade >= 80 && grade <= 83) {
//         alert("Grade: B-");
//     }
//     else if (grade >= 77 && grade <= 79) {
//         alert("Grade: C+");
//     }
//     else if (grade >= 74 && grade <= 76) {
//         alert("Grade: C");
//     }
//     else if (grade >= 70 && grade <= 73) {
//         alert("Grade: C-");
//     }
//     else if (grade >= 67 && grade <= 69) {
//         alert("Grade: D+");
//     }
//     else if (grade >= 64 && grade <= 66) {
//         alert("Grade: D");
//     }
//     else if (grade >= 60 && grade <= 63) {
//         alert("Grade: D-");
//     }
//     else if (grade <= 59) {
//         alert("Grade: F");
//     }

/**
 * TODO:
 * Create a time system (MILITARY TIME) using if/else if/else statement
 * OR switch case
 * < 12 = 'Good morning'
 * < 18 = 'Good afternoon'
 * something for 'Good evening
 **/

// // from WT:
// function time(x) {
//
//     if (x >= 600 && x <= 1159) {
//         alert("Good Morning");
//     } else if (x >= 1200 && x <= 1759) {
//         alert("Good Afternoon");
//     } else if (x >= 1800 && x <= 2359) {
//         alert("Good Evening");
//     }
// }
// time(1801);
/**
 * TODO:
 * Create a season system using if/else if/else statement
 * EX. 'December - February = Winter', etc.
 **/

// // from WT:
//
// function seasons (month) {
//     if (month==="December" || month==="January" || month==="February") {
//         alert("Winter is here!");
//     }
//     else if (month==="March" || month==="April" || month==="May") {
//         alert("Spring is here!");
//     }
//     else if (month==="June" || month==="July" || month==="August") {
//         alert("Summer is here!");
//     }
//     else if (month==="September" || month==="October" || month==="November") {
//         alert("Fall is here!");
//     }
//
// } seasons("October");


// SWITCH STATEMENT NOTES
// another method for writing conditional statements

// example
// var color = "Blue";
//
// switch ( color ) {
//     case "Red":
//         alert(" You chose Red! ");
//         break;
//     case "Blue":
//         alert(" You chose Blue! ");
//         break;
//     case "Green":
//         alert(" You chose Green! ");
//         break;
//     default:
//         alert("You didn't select a cool color");
//         break;
// }


// SWITCH CASE VERSION:

// 1
//
// var input = confirm("Shall we play a game?");
//
// switch (input) {
//
//     case false:
//
//         alert("Maybe next time then.");
//
//         break;
//
//     case true:
//
//         var num = prompt("Let's start by entering a number other than zero... please:");
//
//         switch (num) {
//
//             case "":
//
//                 alert("You did not enter a number. I know you can do this. Please refresh the page to try again.");
//
//                 break;
//
//             default:
//
//                 switch (num) {
//
//                     case "0":
//
//                         alert("Remember... a number other than zero... Please try again.");
//
//                         var num = prompt("Please enter a number other than zero.");
//
//                         switch (num) {
//
//                             case "0":
//
//                                 alert("COME ON! You know what? Come back when you learn to follow instructions. (UNBELIEVABLE!)")
//
//                                 break;
//                         }
//
//                         break;
//
//                 }
//
//                 switch (num % 2 == 0) {
//
//                     case true:
//
//                         alert ("The number " + num + " is an even number.");
//
//                         var num1 = Number(num) + 100;
//
//                         alert (num + " plus 100 equals " + num1);
//
//                         break;
//
//                     default:
//
//                         alert ("The number " + num + " is an odd number.");
//
//                         var num1 = Number(num) + 100;
//
//                         alert (num + " plus 100 equals " + num1);
//
//                         break;
//                 }
//
//                 switch (num < 0) {
//
//                     case true:
//
//                         alert ("The number " + num + " is negative.");
//
//                         break;
//
//                     case false:
//
//                         alert ("The number " + num + " is positive.");
//
//                         break;
//
//                 }
//         }
// }

// 4

var score = prompt ("Please enter your test score in points (ex. 99, 72, etc.):")

switch (score >= 97) {

    case true:

        alert("Grade: A+");

        break;

}

switch (score <= 96) {

    case true:

        switch (score >= 94) {

            case true:

                alert("Grade: A");

                break;

        }

}

switch (score <= 93) {

    case true:

        switch (score >= 90) {

            case true:

                alert("Grade: A-");

                break;

        }

}

switch (score <= 96) {

    case true:

        switch (score >= 94) {

            case true:

                alert("Grade: B");

                break;

        }

}

switch (score <= 93) {

    case true:

        switch (score >= 90) {

            case true:

                alert("Grade: B-");

                break;

        }

}

// 5

// var time = prompt ("Please enter the current time in 24 hr format (ex. 600 for 6:00 am, 2100 for 9:00 pm:")
//
// switch (time >= 600) {
//
//     case true:
//
//         switch (time <= 1159) {
//
//             case true:
//
//                 alert("Good Morning");
//
//                 break;
//
//         }
//
// }
//
// switch (time >= 1200) {
//
//     case true:
//
//         switch (time <= 1759) {
//
//             case true:
//
//                 alert("Good Afternoon");
//
//                 break;
//
//         }
//
// }
//
// switch (time >= 1800) {
//
//     case true:
//
//         switch (time <= 2359) {
//
//             case true:
//
//                 alert("Good Evening");
//
//                 break;
//
//         }
//
// }

// 6

// var month = prompt("Please enter a month of the year (ex. April):")
//
// switch (month) {
//
//     case "March":
//         alert("Spring is here!");
//         break;

//     case "April":
//         alert("Spring is here!");
//         break;

//     case "May":
//         alert("Spring is here!");
//         break;

//     case "June":
//         alert("Summer is here!");
//         break;

//     case "July":
//         alert("Summer is here!");
//         break;

//     case "August":
//         alert("Summer is here!");
//         break;

//     case "September":
//         alert("Fall is here!");
//         break;

//     case "October":
//         alert("Fall is here!");
//         break;

//     case "November":
//         alert("Fall is here!");
//         break;

//     case "December":
//         alert("Winter is here!");
//         break;

//     case "January":
//         alert("Winter is here!");
//         break;

//     case "February":
//         alert("Winter is here!");
//         break;

//     default:
//         alert("You did not enter a month.");
//         break;
//
// }