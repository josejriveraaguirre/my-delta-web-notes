
// WHILE LOOP

// var number = 0;
// while(number <= 100) {
//
//     console.log(`# ${number}`);
//
//     number += 3;
//
// }

// DO WHILE LOOP

// var x = 10;
//
// do {
//
//     console.log(`do-while loop # ${x}`);
//
//     x--;
//
// } while (x >= 0);

// FOR LOOP

// for (var x = 100; x <= 200; x += 10) {
//     console.log("for loop # " + x);
// }

// BREAK AND CONTINUE

// var endAtNumber = 5;
//
// for (var i = 1; i<= 100; i ++) {
//     console.log(`loop count # ${i}`);
//
//     if (i=== endAtNumber) {
//         alert("We have reached our break!");
//         break;
//         console.log("Do you see this message?);")
//     }
// }

// for (var n = 1; n <= 100; n++) {
//
//     if (n % 2!==0) {
//         continue;
//     }
//     console.log ("Even Number: " + n);
// }

// while loop practice
//
// var num = 2;
// while (num <= 65536) {
//
//     console.log(num);
//
//     num *= 2;
//  }

// SHORTCUTS

// num *= 2
// num /= 2
// num -= 2
// num += 2