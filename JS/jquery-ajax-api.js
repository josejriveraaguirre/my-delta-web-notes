
"use strict";

$.get("https://jsonplaceholder.typicode.com/users", {

}).done(function (data) {

    // console.log(data);


    // console.log(data[7].name);


    // console.log(data[0].address.city);

var renderData = displayUsers(data);

$("#users").html(renderData);

});

function displayUsers(users) {

  var usersOnHTML = "";

  users.forEach(function (user) {

        usersOnHTML += `
        
        <div class="user">
        
            <h3>Employee Name: ${user.name}</h3>
            
            <a href="http://${user.website}">View website</a>
            
            <p>User Info:</p>
            
            <ul>
            
                <li>
                
                <p>Username: ${user.username}</p>
                
                </li>
                
                <li>
                
                <p>Address: ${user.address.street} ${user.address.city}</p>
                
                </li>
            
            </ul>
        
        </div>   
        
        `

  });

  return usersOnHTML;

};