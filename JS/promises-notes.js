"use strict";

//PROMISES
// TOOL FOR HANDLING ASYNCHRONOUS EVENTS
//three states:
//pending - event not happened
//resolved - event happened
//rejected - event happened, but an error happened

// var isMomHappy = false;
//
// var willIGetANewPhone = new Promise(function (resolve, reject) {
//
//     if (isMomHappy === true) {
//
//         var phone = {
//
//             brand: "Samsung",
//
//             color: "Black"
//
//         };
//
//         resolve(phone);
//     }
//
//     else {
//
//         var reason = new Error("Your grades are not good!");
//
//         reject(reason);
//     }
//
// });

// console.log(willIGetANewPhone);

// var promise = new Promise(function (resolve, reject) {
//
//     setTimeout(function () {
//         // resolve("Promise is resolved.")
//         reject("Promise has been rejected.")
//     }, 5000)
//
// });
//
// console.log(promise);

// let goodKid = true;
//
// //create a promise
//
// const getsCake = new Promise(function (resolve, reject) {
// if (goodKid === true) {
//     resolve("Here is a piece of chocolate cake");
// }
// else {
//     reject("No cake. You're misbehaving");
// }
// })
//
// .then(data => console.log(data))
// .catch(error => console.log(error));
//
// console.log(getsCake);

// $.get('https://swapi.dev/api/people/1')
//     .then(function (data) {
//         console.log(data);
//     })
//
//     .catch(function (error) {
//         console.log(error);
//     });

// let makeOrder = new Promise((resolve, reject) => {
//
//         setTimeout(function () {
//
//             resolve(`Coffee type: Espresso is ready!`);
//
//         }, 2000);
//
//     });
//
// console.log(makeOrder);

// let processOrder = new Promise((resolve, reject) => {
//
//     setTimeout(function () {
//
// resolve("Coffee type Espresso has been processed and paid for");
//
//     }, 4000);
//
// });
//
// console.log(processOrder);

const starbucksOrder = type => {

let makeOrder = new Promise((resolve, reject) => {

        setTimeout(function () {

            resolve(`Your ${type} is ready!`);

        }, 2000);

    });

let processOrder = new Promise((resolve, reject) => {

    setTimeout(function () {

resolve(`Your ${type} has been processed and paid for.`);

    }, 4000);

});

    let enjoy = new Promise((resolve, reject) => {

        setTimeout(function () {

            resolve(`Enjoy your ${type}!`);

        }, 6000);

    });

return Promise.all([processOrder, makeOrder, enjoy]);

}

starbucksOrder("espresso").then((data) => {
    console.log(data[0]);
    console.log(data[1]);
    console.log(data[2])
}).catch((error) => console.log(error));
