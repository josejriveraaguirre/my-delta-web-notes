/* JAVASCRIPT ARRAYS */
// Complete the following in a new js file named arrays-exercise.js
"use strict";
/** TODO:
/**  Create a new EMPTY array named sports.**/
/** Push into this array 5 sports**/
/** Sort the array.**/
/** Reverse the sort that you did on the array.**/
/** Push 3 more sports into the array **/
/** Locate your favorite sport in the array and Log the index. **/
/** Remove the 4th element in the array **/
/** Remove the first sport in the array **/
/** Turn your array into a string  */


// var sports = [];

// console.log(sports);

// sports.push("basketball", "baseball", "soccer", "hockey", "boxing");

// console.log(sports);

// console.log(sports.sort());

// console.log(sports.reverse(sports.sort()));

// sports.push("skiing", "skateboarding", "surfing");

// console.log(sports);

// console.log(sports.indexOf("skateboarding"));

// sports.splice(3,1);

// console.log(sports);

// sports.shift();

// console.log(sports);

// console.log(sports.sort());

//from WT:

// console.log(sports.join(", "));



 /** TODO: Create an array to hold your top singers/bands/etc.
 *  const bands = ['AC/DC', 'Pink Floyd', 'Van Halen', 'Metallica'];
 1. console log 'Van Halen'
 2. add 'Fleetwood Mac' to the beginning of the array
 3. add 'Journey' to the end of the array
 */

// const bands = ['AC/DC', 'Pink Floyd', 'Van Halen', 'Metallica'];

// console.log(bands[2]);

// bands.unshift("Fleetwood Mac");

// console.log(bands);

// bands.push("Journey");

// console.log(bands);


/** Create a new array named cohort_name that holds the names of the people in your class**/
/** Log out each individual name from the array by accessing the index. **/
/** Using a for loop, Log out every item in the COHORT_NAME array **/
/** Refactor your code using a forEach Function **/


// var cohort_name = ["Angela", "Alyssa", "Victor", "Jose"];

// console.log(cohort_name[0]);
// console.log(cohort_name[1]);
// console.log(cohort_name[2]);
// console.log(cohort_name[3]);

// for (var i=0; i< cohort_name.length; i++) {
//     console.log(cohort_name[i]);
// }

// cohort_name.forEach( function (student_name) {
//     console.log(student_name);
// });


/** Using a for loop. Remove all instance of 'CodeBound' in the array **/


// var arrays1 = ["fizz", "CodeBound", "buzz", "fizz", "CodeBound", "buzz"];
//
// for ( var i = 0; i < arrays1.length; i++) {
//
//     switch (arrays1 [i] == "CodeBound") {
//
//         case true:
//
//             console.log(arrays1);
//
//             arrays1.splice(i, 1);
//
//             break;
//
//     }
//
// }
//
// console.log(arrays1);

/**Using a forEach loop. Convert them to numbers then sum up all the numbers in the array **/


// var numbers = [3, '12', 55, 9, '0', 99];
//
// console.log(numbers);
//
// numbers.forEach(function (number) {
//
//    console.log(numbers.indexOf(number));
//     // numbers.map(Number(number));
//
//    // switch  (isNaN(number)) {
//    //
//    //     case true:
//    //
//    //         var indX = numbers.indexOf(number);
//    //         var newVal = Number(number);
//    //
//    //         numbers.splice(indX,1, newVal);
//    //         break;
//    //
//    // }
//
//     // number = Number(number);
//
//     // numbers.splice(1-numbers.indexOf(), 1, Number(numbers));
// });
// console.log(numbers);

// from WT:

// var numbers = [3, '12', 55, 9, '0', 99];
//
// var sum = 0;
//
// numbers.forEach(function(number) {
//
//     sum+= parseInt(number);
//
// });
// console.log(sum);


/**Using a forEach loop Log each element in the array while changing the value to 5 times the original value **/


// var numbers = [7, 10, 22, 87, 30];
//
// console.log(numbers);
//
// numbers.forEach(function (number) {
//     console.log(number*5);
// });


/** Create a function named removeFirstLast where you remove the first and last character of a string.**/

var str = "Delta";

var letters = str.split("");

console.log(letters);

function removeFirstLast(array) {

letters.shift(array);

letters.pop(array);

// from WT: return letters.join("");

}

removeFirstLast(letters);

console.log(letters);



// Bonus
/** Write a function named phoneNum that accepts an array of 10 integers (between 0 and 9). This will return a string of those numbers in a
 *  phone number format.
 *  phoneNum([7,0,8,9,3,2,0,1,2,3])=> returns "(708) 932-0123"
 */
/** Create a function that will take in an integer as an argument and Returns "Even" for even numbers and returns "Odd" for odd numbers **/
/** Create a function named reverseString that will take in a string and reverse it. There are many ways to do this using JavaScript built-in
 * methods. For this exercise use a loop.
 */
/**Create a function named converNum that will convert a group of numbers to a reversed array of numbers**
 * 386543 => [3,4,5,6,8,3]
 */
/**white_check_mark
eyes
raised_hands*/





